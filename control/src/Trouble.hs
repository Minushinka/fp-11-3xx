{-==== Минушина Рина, 11-304 ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}

ratMax :: [(Integer, Integer)] -> (Integer, Integer)
ratMax (isOne:[]) = isOne
ratMax (isOne:isTwo) = if ((fst isOne) `quot` (snd isOne)) > (fst (ratMax isTwo)) `quot` (snd (ratMax isTwo)) then isOne
    else ratMax isTwo


getMin :: [(Integer, Integer)] -> Integer
getMin (isOne:[]) = snd isOne
getMin (isOne:isTwo) | snd isOne < getMin isTwo = snd isOne
                  | otherwise = getMin isTwo


rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket isTwo = if snd (ratMax isTwo) == getMin isTwo then [ratMax isTwo] else isTwo  

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}
getOrbit :: [Load a] -> [Load a]
getOrbit [] = []
getOrbit ((Orbiter isOne):isTwo) = (Orbiter isOne):(getOrbit isTwo)
getOrbit ((Probe isOne):isTwo) = getOrbit isTwo

orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters ((Cargo isOne):isTwo) = (orbiters isTwo) ++ (getOrbit isOne) 
orbiters ((Rocket _ isOne):isTwo) = (orbiters isTwo) ++ (orbiters isOne)
{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier [] = []
finalFrontier ((Warp _):isTwo) = "Kirk":(finalFrontier isTwo)
finalFrontier ((BeamUp _):isTwo) = "Kirk":(finalFrontier isTwo)
finalFrontier ((IsDead _):isTwo) = "McCoy":(finalFrontier isTwo)
finalFrontier ((LiveLongAndProsper):isTwo) = "Spock":(finalFrontier isTwo)
finalFrontier ((Fascinating):isTwo) = "Spock":(finalFrontier isTwo)