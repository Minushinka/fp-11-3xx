-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval (Const x) = x
eval (Add t1 t2) = eval t1 + eval t2
eval (Sub t1 t2) = eval t1 - eval t2
eval (Mult t1 t2) = eval t1 * eval t2

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Const x) = Const x
simplify (Mult (Add x y) z) = simplify (Add (Mult (simplify x) (simplify z)) (Mult (simplify y) (simplify z)))
simplify (Mult (Sub x y) z) = simplify (Sub (Mult (simplify x) (simplify z)) (Mult (simplify y) (simplify z)))
simplify (Mult z (Add x y)) = simplify (Add (Mult (simplify z) (simplify x)) (Mult (simplify z) (simplify y)))
simplify (Mult z (Sub x y)) = simplify (Sub (Mult (simplify z) (simplify x)) (Mult (simplify z) (simplify y)))
simplify (Mult x y) = Mult (simplify x) (simplify y)
simplify (Add x y)  = Add  (simplify x) (simplify y)
simplify (Sub x y)  = Sub  (simplify x) (simplify y)

