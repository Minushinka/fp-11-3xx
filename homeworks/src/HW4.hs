﻿-- Срок: 2016-04-02 (100%), 2016-04-07 (50%)

module HW4
       ( Show' (..)
       , A(..)
       , C(..)
       , Set(..)
       , symmetricDifference
       , fromBool
       , fromInt
       ) where

class Show' a where
  show' :: a -> String

data A = A Int
       | B
-- show' (A 5)    == "A 5"
-- show' (A (-5)) == "A (-5)"
-- show' B        == "B"

data C = C Int Int
-- show' (C 1 2)       == "C 1 2"
-- show' (C (-1) (-2)) == "C (-1) (-2)"

instance Show' A where
      show' (A val) = "A " ++ show' val
      show' B = "B"

instance Show' C where
      show' (C val1 val2) = "C " ++ show' val1 ++ " " ++ show' val2

instance Show' Int where
      show' val = if val > 0 then show val else "(" ++ show val ++ ")" 
----------------

data Set a = Set (a -> Bool)

-- Симметрическая разность -
-- элементы обоих множеств, не входящие в объединение
-- {4,5,6} \/ {5,6,7,8} == {4,7,8}
symmetricDifference :: Set a -> Set a -> Set a
symmetricDifference (Set a) (Set b) = Set $ \x -> (a x || b x) && not (a x && b x)

-----------------

-- fromBool - переводит булевское значение в кодировку Чёрча
fromBool val = if val then \t -> (\f -> t) else \t f -> f

-- fromInt - переводит число в кодировку Чёрча
fromInt val = if val == 0 then \s z -> z else \s z -> s (fromInt (val - 1) s z)

